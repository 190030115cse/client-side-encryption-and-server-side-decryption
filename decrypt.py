from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

from typing import Union
import math

app = FastAPI()

class ClientData(BaseModel):
    key: Union[str, None] = None
    endata: Union[str, None] = None

@app.post("/decrypt_pwd/")
async def decrypt_pwd(encrypted_data: ClientData):
    try:
        username = encrypted_data.key
        encryptedText = encrypted_data.endata

        encryptedTextLength = len(encryptedText)

        ASCII_Username = []
        for i in username:
            ASCII_Username.append(ord(i))

        ASCII_Username_Sum = list(map(int, str(sum(ASCII_Username))))

        for i in range(len(ASCII_Username_Sum)):
            if ASCII_Username_Sum[i] == 0:
                ASCII_Username_Sum[i] = 1

        max_ASCII_Username_Sum = max(ASCII_Username_Sum)

        def findLargest(arr):
            a=arr
            a=list(set(a))
            a.sort()
            if(len(a)==1 ):
                return (a[0]+1)
            else:
                return (a[-2])

        second_largest = findLargest(ASCII_Username_Sum)

        if second_largest == 0 or math.isinf(second_largest) or second_largest == -math.inf or second_largest == max_ASCII_Username_Sum:
            second_largest = max_ASCII_Username_Sum + 1

        lengthUsername10 = len(username) * 10
        password_length = encryptedTextLength / lengthUsername10

        encryptedText_list = []
        for i in range(int(password_length)):
            encryptedText_list.append(encryptedText[i*int(lengthUsername10):(i+1)*int(lengthUsername10)])

        randomDigits = []
        for i in range(len(encryptedText_list)):
            randomDigits.append(encryptedText_list[i][-second_largest])

        HexList = []
        for i in range(len(encryptedText_list)):
            HexList.append(encryptedText_list[i][int(randomDigits[i])]+encryptedText_list[i][int(randomDigits[i])+1])

        final_list = []
        for i in range(len(HexList)):
            final_list.append(chr(int(HexList[i], 16)))

        Plain_password = []
        for i in final_list:
            value = max_ASCII_Username_Sum + int(max(randomDigits))
            Plain_password.append(chr(ord(i) - value))
        decipherText = "".join(Plain_password)

        data = {
            'MetaKey': decipherText,
        }
        return data
    except Exception as e:
        return str(e)

origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)